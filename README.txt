demo-pyramid README
==================

Getting Started
---------------

- cd <directory containing this file>

- $VENV/bin/pip install -e .

- $VENV/bin/initialize_demo-pyramid_db development.ini

- $VENV/bin/pserve development.ini

